stepreduce: stepreduce.cpp
	c++ -O3 -Wall -Wextra -std=c++17 -lstdc++ -o $@ $^

debug: stepreduce.cpp
	c++ -O0 -Wall -Wextra -std=c++17 -ggdb -lstdc++ -o $@ $^

.PHONY: clean all default
all: stepreduce
default: all
clean:
	rm -f stepreduce
